appAPI.ready(function($) {

  // If there is other stuff than ignore
  $('p').each(function(){
    if(!$.trim($(this).html()) == '') $(this).addClass('full-of-space');
  });


  // Finds unwrapped elements
  function getTextNodesIn(node, includeWhitespaceNodes) {
  var textNodes = [], whitespace = /^\s*$/;

  function getTextNodes(node) {
    if (node.nodeType == 3) {
      if (includeWhitespaceNodes || !whitespace.test(node.nodeValue)) {
        textNodes.push(node);
      }
    } else {
      for (var i = 0, len = node.childNodes.length; i < len; ++i) {
        getTextNodes(node.childNodes[i]);
      }
    }
  }

  getTextNodes(node);
  return textNodes;
  }

  var textnodes = getTextNodesIn($("div")[0]);
  for(var i=0; i < textnodes.length; i++){
    if($(textnodes[i]).parent().is("div")){
      $(textnodes[i]).wrap("<span class='needs-wrapping'>");
    }
  }

  // Style the output
  function addStyle (css) {
    var head, styleElement;
    head = document.getElementsByTagName('head')[0];
    styleElement = document.createElement('style');
    styleElement.setAttribute('type', 'text/css');
    if (styleElement.styleSheet) {
      styleElement.styleSheet.cssText = css;
    } else {
      styleElement.appendChild(document.createTextNode(css));
    }
    head.appendChild(styleElement);
    return styleElement;
  }

  var css = "p:empty{display:block;border:2px dashed #ff00ff;min-height:15px;margin:5px}p:empty:before{content:'[- - empty <p> - -]';color:#ff00ff;font-weight:700;font-family:monospace}.full-of-space{display:block;border:2px dashed orange;min-height:15px;margin:5px}.full-of-space:before{content:'[- - empty <p> full of spaces - -]';color:orange;font-weight:700;font-family:monospace}.needs-wrapping{display:block;border:2px dashed #0ff;min-height:15px;margin:5px}.needs-wrapping:before{content:'[- - unwrapped text - -]';color:#0ff;font-weight:700;font-family:monospace}";
  addStyle(css);

});
